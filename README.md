# Hoe werkt Git?
Het is verplicht om je "huiswerk" iedere week te pushen naar Gitlab. Dit zodat we jouw code kunnen bespreken tijdens de les.

Om te beginnen heb je het volgende commando nodig:
```
git clone https://gitlab.com/frieslandcollege/workshop/NAAM.git
```

Op deze manier haal je je repository veilig binnen zodat je straks geen last krijgt van merge conflicts i.v.m. de README.md die standaard in je project zit.

Het is belangrijk dat je de structuur van je project goed opbouwt. Je dient je aan de volgende standaard te houden:

```
NAAM (Naam van je Repository die je hebt gecloned vanaf gitlab)
   - .git (folder)
   - Project 1 (folder)
   - Project 2 (folder)
   - Project 3 (folder)
   - Readme.md (bestand)
```
In je windows verkenner zou het er dus ongeveer zo uit moeten zien:
![alt text](https://img.mlgeditz.nl/images/ysltvy7e2q2mxgoba2g1aqmqzcouns82.png)

Om iets te pushen doe je de volgende commando's:
```
git status //Check of alles goed is gegaan
git add . //Voeg alle folders toe
git commit -m "Commit message" //Maak een nieuwe commit
git pull //Pull everything from your repository incase something changed there
git push //Push your local project to git
```
In je terminal ziet dat er zo uit:

![alt text](https://img.mlgeditz.nl/images/c5prf2p6ea5m6oc7igfjk8m1svpar21h.png)

Wees er zeker van dat je in de folder zit van je eigen repository (NAAM).
Op die manier zou je project er ongeveer zo uit moeten zien in gitlab:

![alt text](https://img.mlgeditz.nl/images/imkuitgrebtpaus0hz9qsdskpga4p4lp.png)

Als alles er zo uitziet heb je het goed gedaan en kun je zonder zorgen verder werken.
Succes!

#Snippets
Wanneer iedereen zijn/haar opdracht heeft ingeleverd wordt er een example online gezet. Deze kun je vinden onder [Snippets](https://gitlab.com/frieslandcollege/workshop/general/snippets).

#Opdrachten
Iedere week krijgen jullie een nieuwe opdracht. Deze kunnen jullie [hier](https://gitlab.com/frieslandcollege/workshop/general/issues) vinden.
Wanneer er een nieuwe opdracht beschikbaar is kun je die vinden onder het mapje [**Open**](https://gitlab.com/frieslandcollege/workshop/general/issues?scope=all&utf8=%E2%9C%93&state=opened). Oude opdrachten kun je vinden bij [**Closed**](https://gitlab.com/frieslandcollege/workshop/general/issues?scope=all&utf8=%E2%9C%93&state=closed).

Als je je opdracht af hebt kun je op de desbetreffende opdracht reageren met een duimpje omhoog.





