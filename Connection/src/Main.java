import objects.Animal;

public class Main {

    public static void main(String[] args) {
        /* ===============================================
         * Connect to MySQL Server
         * ===============================================
         */
        Connection connection = new Connection(); //Create new Connection object
        connection.connect(); //Open connection to the MySQL Server


        /* ===============================================
         * Add new Animal into the database
         * ===============================================
         */

        //Create new animal.
        Animal cAnimal = new Animal(); //Create new animal object.
        cAnimal.setID(1); //Set the id of the animal to 1
        cAnimal.setName("Johan"); //Set the name of the animal to "Johan"
        cAnimal.setAge(6); //Set the age of the animal to 6
        cAnimal.setSpecie("Capibara"); //Set the specie of the animal to "Capibara"
        cAnimal.setWeight(70); //Set the weight of the animal to 70

        connection.createAnimal(cAnimal); //Put the animal into the database.


        /* ===============================================
         * Get Animal from the database
         * ===============================================
         */
        Animal rAnimal = connection.getAnimal(1); //Ask database for the data of animal with id 1 and put into an variable.

        //Should return Your Capibara his name is Johan, is 6 years old and weights 70kg.
        System.out.println("Your " + rAnimal.getSpecie() + " his name is " + rAnimal.getName() + ", is " + rAnimal.getAge() + " years old and weights " + rAnimal.getWeight() + "kg.");



        /* ===============================================
         * Updates data of an Animal in the database
         * ===============================================
         */
        Animal uAnimal = connection.getAnimal(1);//Ask database for the data of animal with id 1 and put into an variable.
        uAnimal.setAge(7); //Set age of the Animal to 7
        uAnimal.setWeight(80); //Set weight og the Animal to 80kg

        connection.updateAnimal(uAnimal, 1); //Update the animal data and put it in the database.

        uAnimal = connection.getAnimal(1); //Update the value of uAnimal to the new data.

        //Should return Your Capibara his name is Johan, is 7 years old and weights 80kg.
        System.out.println("Your " + uAnimal.getSpecie() + " his name is " + uAnimal.getName() + ", is " + uAnimal.getAge() + " years old and weights " + uAnimal.getWeight() + "kg.");



        /* ===============================================
         * Delete data from the database
         * ===============================================
         */
        connection.deleteAnimal(1); //Delete animal with ID 1 from the database.



        /* ===============================================
         * Close the connection to the MySQL Server
         * ===============================================
         */
        connection.close();
    }

}
