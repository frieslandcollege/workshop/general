import objects.Animal;

import java.sql.*;


public class Connection {

    /**
     * Services used by the class to interact with the MySQL Server.
     *
     * (SERVICE)                            (PURPOSE)
     * ===========================================================================
     * - Connection          []   (Stores the connection object)
     * - Statement           []   (Stores the statement object)
     *
     * - Host                []   (Stores the host to the MySQL Server)
     * - Port                []   (Stores the port to the MySQL Server)
     * - Database            []   (Stores the database to the MySQL Server)
     * - Username            []   (Stores the username of the MySQL Server)
     * - Password            []   (Stores the password of the MySQL Server)
     */
    private java.sql.Connection connection;
    private Statement statement;

    private String host = "localhost";
    private String port = "3306";
    private String database = "fc-workshop";
    private String username = "root";
    private String password = "";

    /**
     * Opens a new connection to the MySQL Server.
     * The function doesn't throw any exceptions, but will log an error.
     */
    public void connect() {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver"); //Hooks jdbc MySQL driver into the class.
            this.connection = DriverManager.getConnection("jdbc:mysql://" + host + ":" + port + "/" + database, username, password); //Opens connection to the MySQL Server
            System.out.println("Openend mysql connection successfully!"); //Tell the user everything worked correctly.

            this.statement = connection.createStatement(); //Create a new statement and put it in the variable.
        } catch (ClassNotFoundException | SQLException e) {
            System.out.println("Something went wrong while opening mysql connection: " + e.getMessage()); //When something goes wrong throw an error to the user.
        }
    }

    /**
     * Adds an new animal to the database by getting the information of an Animal object.
     * This Object contains:
     * - ID
     * - Name
     * - Age
     * - Specie
     * - Weight
     * The function will get all this information from the object and put it into the database.
     * The function doesn't throw any exceptions, but will log an error.
     *
     * @param animal - Animal object containing all the data of the animal.
     */
    public void createAnimal(Animal animal) {
        try {
            //Get all data from the Animal object.
            Integer id = animal.getID(); //Get ID of the animal and store it in an variable.
            String name = animal.getName(); //Get Name of the animal and store it in an variable.
            Integer age = animal.getAge(); //Get Age of the animal and store it in an variable.
            String specie = animal.getSpecie(); //Get Specie of the animal and store it in an variable.
            Integer weight = animal.getWeight(); //Get Weight of the animal and store it in an variable.

            //Execute an new update to the statement.
            statement.executeUpdate("INSERT INTO `workshoptable` (`ID`, `name`, `age`, `specie`, `weight`) VALUES ('" + id + "', '" + name + "', '" + age + "', '" + specie + "', '" + weight + "')");

            System.out.println("Created new " + specie + " called " + name); //Tell the user everything went fine.
        } catch (SQLException e) {
            System.out.println("Something went wrong while creating animal: " + e.getMessage()); //When something goes wrong throw an error to the user.
        }
    }

    /**
     * Searches database to get information about an animal by searching for the given ID.
     * The returned Animal Object contains:
     * - ID
     * - Name
     * - Age
     * - Specie
     * - Weight
     * The function doesn't throw any exceptions, but will log an error.
     *
     * @param id - ID of the animal the user want to get information of
     * @return animal - Object containing all Animal data.
     */
    public Animal getAnimal(Integer id) {
        try {
            //Create a new prepared statement
            PreparedStatement pst = connection.prepareStatement("SELECT * FROM `workshoptable` WHERE `ID` = '" + id + "'");
            ResultSet result = pst.executeQuery(); //Execute the PreparedStatement and put the results in an ResultSet object.

            //Check for all results
            if (result.next()) {
                Animal animal = new Animal(); //Create an new Animal object to put all the data in
                animal.setID(result.getInt("ID")); //Get the ID from the result and put it in the Animal object.
                animal.setName(result.getString("name")); //Get the Name from the result and put it in the Animal object.
                animal.setAge(result.getInt("age")); //Get the Age from the result and put it in the Animal object.
                animal.setSpecie(result.getString("specie")); //Get the Specie from the result and put it in the Animal object.
                animal.setWeight(result.getInt("weight")); //Get the Weight from the result and put it in the Animal object.

                return animal; //Return the animal object
            }
        } catch (SQLException e) {
            System.out.println("Something went wrong while getting animal: " + e.getMessage()); //When something goes wrong throw an error to the user.
        }

        return null; //When something goes wrong in the process. Nothing(NULL) will be returned.
    }


    /**
     * Updates an existing animal from the database by getting the information of an Animal object.
     * This Object contains:
     * - ID
     * - Name
     * - Age
     * - Specie
     * - Weight
     * The function will update all information of the animal. Even if it is the same.
     * The function doesn't throw any exceptions, but will log an error.
     *
     * @param animal - Animal object containing all the data of the animal.
     * @param id - ID of the animal a user wants to update.
     */
    public void updateAnimal(Animal animal, Integer id) {
        try {
            //Get all data from the Animal object.
            Integer newID = animal.getID(); //Get ID of the animal and store it in an variable.
            String name = animal.getName(); //Get Name of the animal and store it in an variable.
            Integer age = animal.getAge(); //Get Age of the animal and store it in an variable.
            String specie = animal.getSpecie(); //Get Specie of the animal and store it in an variable.
            Integer weight = animal.getWeight(); //Get Weight of the animal and store it in an variable.

            //Execute an new update to the statement.
            statement.executeUpdate("UPDATE `workshopTable` SET `id`='" + newID + "', `name`='" + name + "', `age`='" + age + "', `specie`='" + specie + "', `weight`='" + weight + "' WHERE `ID`='" + id + "'");

            System.out.println("Updated animal with id " + id); //Tell the user everything went successful.
        } catch (SQLException e) {
            System.out.println("Something went wrong while updating an animal: " + e.getMessage()); //When something goes wrong throw an error to the user.
        }
    }

    /**
     * Deletes an animal from the database by its ID.
     * The function will delete all the data assigned to the given id.
     * The function doesn't throw any exceptions, but will log an error.
     *
     * @param id - ID of the animal a user wants to delete.
     */
    public void deleteAnimal(Integer id) {
        try {
            //Execute an new update to the statement.
            statement.executeUpdate("DELETE FROM `workshopTable` WHERE `ID`='" + id + "'");

            System.out.println("Deleted animal with id " + id + " successfully"); //Tell the user the animal is deleted
        } catch (SQLException e) {
            System.out.println("Something went wrong while deleting an animal: " + e.getMessage()); //When something goes wrong throw an error to the user.
        }
    }

    /**
     * Closes the connection to the MySQL Server.
     * The function will clear both the connection and statement variables so no other queries can be runned.
     */
    public void close() {
        try {
            connection.close(); //Close the connection to the database.
            connection = null; //Set the value of "connection" to null, so the user can't interact with it anymore.
            statement = null; //Set the value of "statement" to null, so the user can't interact with it anymore.
            System.out.println("MySQL Connection closed successfully!"); //Tell the user the connection was closed.
        } catch (SQLException e) {
            System.out.println("Something went wrong while closing the connection: " + e.getMessage()); //When something goes wrong throw an error to the user.
        }

    }

}
