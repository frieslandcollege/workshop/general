package objects;

public class Animal {

    private Integer id;
    private String name;
    private Integer age;
    private String specie;
    private Integer weight;

    public void setID(Integer id) {
        this.id = id;
    }

    public Integer getID() {
        return this.id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getAge() {
        return this.age;
    }

    public void setSpecie(String specie) {
        this.specie = specie;
    }

    public String getSpecie() {
        return this.specie;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public Integer getWeight() {
        return this.weight;
    }



}
