import java.io.IOException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        clear();
        Scanner input = new Scanner(System.in);

        System.out.println("Hoeveel boodschappen wil je halen?");
        int listLength = Integer.parseInt(input.nextLine());
        clear();

        String[] array = new String[listLength];
        for (int i = 0; i < array.length; i++) {
            System.out.println("Voeg nieuw product toe (" + (i+1) + "/" + (array.length) + "):");
            String product = input.nextLine();
            array[i] = product;

            clear();
        }

        System.out.println("Dit is je boodschappenlijst: ");
        System.out.println("-=+=-");
        for (int i = 0; i < array.length; i++) {
            System.out.println( (i+1) + ") " + array[i]);
        }
        System.out.println("-=+=-");
    }


    public static void clear() {
        for (int i = 0; i < 50; i++) {
            System.out.println();
        }
    }

}
