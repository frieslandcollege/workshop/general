import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        //Create new scanner to read userinput.
        Scanner input = new Scanner(System.in);

        System.out.println("Enter first number...");
        double firstNumber = Double.parseDouble(input.nextLine()); //Get user input as String, convert it to an Double and store it in an variable.

        System.out.println("Enter second number...");
        double secondNumber = Double.parseDouble(input.nextLine()); //Get user input as String, convert it to an Double and store it in an variable.

        System.out.println("Enter operator...");
        String operator = input.nextLine(); //Get operator from user input and store is as a String in an variable.

        System.out.println("Calculating sum '" + firstNumber + " " + operator + " " + secondNumber + "'");
        double answer = 0; //Create a new variable to store the answer (This way the answer will be 0 if something breaks.

        //Switch statement to create the sum according to the operator.
        switch (operator) {
            //Check if operator is +
            case "+":
                answer = firstNumber + secondNumber; //Calculating answer by using +
                break; //Break switch statement to prevent looping any further

            //Check if operator is -
            case "-":
                answer = firstNumber - secondNumber; //Calculating answer by using -
                break; //Break switch statement to prevent looping any further

            //Check if operator is *
            case "*":
                answer = firstNumber * secondNumber; //Calculating answer by using *
                break; //Break switch statement to prevent looping any further

            //Check if operator is /
            case "/":
                answer = firstNumber / secondNumber; //Calculating answer by using /
                break; //Break switch statement to prevent looping any further

            //Check if operator is %
            case "%":
                answer = firstNumber % secondNumber; //Calculating answer by using %
                break; //Break switch statement to prevent looping any further

            //Catch if a valid operator cannot be found.
            default:
                System.out.println("Invalid operator... Try again!");
                break; //Break switch statement to prevent looping any further
        }

        System.out.println("Answer is: " + answer); //Log the answer into the console
    }

}
